import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import sys

# Implement principal component analysis
def doPCA():
	Tstress = np.loadtxt('650T_stress.txt')
	ncomp = 3
	pca = PCA(n_components=ncomp)
	pca.fit(Tstress)
	return pca

# To project data on principal component: projected data is called principal component score.
def transform():
    
    pca = doPCA()
    print(pca.explained_variance_ratio_)
    np.savetxt('650T_PC.txt', pca.components_, fmt='%.6f')
    
    Tstress = np.loadtxt('650T_stress.txt')
    transformed_data = pca.transform(Tstress)
    np.savetxt('650T_score.txt', transformed_data, fmt='%.6f')

    Vstress = np.loadtxt('150V_stress.txt')
    transformed_data = pca.transform(Vstress)
    np.savetxt('150V_score.txt',transformed_data, fmt='%.6f')

# To obtain predictive stress through inverse transfrom of principal compomenent 
def invPCA():
	
	pca = doPCA()
	Mp = np.loadtxt('150V_predictive_score.txt') # predictive mean for the first three principal component scores
	Stress_prediction = pca.inverse_transform(Mp)
	np.savetxt('150V_predictive_stress.txt', Stress_prediction, fmt = '%.6f')

# Below is to plot cumulative explained variance ratio
def CEV():
	
	Tstress = np.loadtxt('650T_stress.txt')
	ncomp = Tstress.shape[1]
	pca = PCA(n_components=ncomp)
	pca.fit(Tstress)
	fig, ax = plt.subplots()
	x = np.linspace(1,Tstress.shape[0],Tstress.shape[0])
	plt.plot(x, 100*np.cumsum(pca.explained_variance_ratio_),'--o', color='blue')
	plt.xlabel('Principal component')
	plt.ylabel('Cumulative percentage explained (%)')
	plt.savefig('T650_cumulativePercentExplained.png')  

if __name__ == '__main__':
	option = sys.argv[1]
	if option == 'transform':
		transform()

	if option == 'CEV':
		CEV()

	if option == 'invPCA':
		invPCA()
