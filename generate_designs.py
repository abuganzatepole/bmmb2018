"""
Generates design points on which you should evaluate the model.

Feel free to change stuff accordingly.

This requires installing pyDOE which you can do by:

    pip install pyDOE


The design we are going to use is called a Latin Hypercube Design.

"""


import pyDOE as pd
import numpy as np


# Specify how many simulations you can afford:
num_simulations = 650


# Specify the lower and upper bounds of your model inputs
# I am not using exactly your numbers. I am rounding up or down.
# The purpose here is to cover the range of possible inputs as well
# as possible.
bounds = np.array([[0.002387, 0.1007],   # [left, right] bounds, C_10 [MPa]
                   [0.000380, 24.530], # k_1 [MPa]
                   [0.133, 161.862]])  # k_2 [-]


# The number of inputs that you have:
num_inputs = bounds.shape[0]

# Now I sample designs in [0,1]^3 and I am going to scale them back to the
# original bounds
X_scaled = pd.lhs(3, num_simulations)

# Let's get to the original space

X = X_scaled * (bounds[:, 1] - bounds[:, 0]) + bounds[:, 0]

# Save to a text file

np.savetxt('650T_input.txt', X, fmt='%.6f')
# You must do all these simulations...
