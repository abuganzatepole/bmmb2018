"""
Builds a regression surface that connects the parameters to the
principal components.
"""
import numpy as np
import matplotlib.pyplot as plt
import GPy # pip install GPy
import numpy.matlib
plt.style.use('classic')

# Parameters
X = np.loadtxt('650T_input.txt')
X[:,0] *=2 # change from C10 to mu

# PCA score
Z = np.loadtxt('650T_score.txt')
# Make sure it is a 2D array

# Do single output GPR
# choose i as the ith principal component score
i = 0 # here, i = 0, 1, and 2, e.g., i=1 indicates the second principal component score.
Z = Z[:,i]
if Z.ndim == 1:
    Z = Z[:, None]

# Let's use Gaussian process regression to connect the two
# We will scale the parameters
X_m = np.mean(X, axis=0)
X_s = np.std(X, axis=0)
X_scaled = (X - X_m) / X_s
# and regress between X_s and Z
k = GPy.kern.RBF(X_scaled.shape[1], ARD=True)
model = GPy.models.GPRegression(X_scaled, Z, k)
model.optimize()
print('*' * 80)
print('Optimized model:')
print(model)
print('Lengthscales:') # Correspond to the sensitivity of the output
                       # to each input (the smaller the lengthscale
                       # of a parameter the more sensitive the response
                       # to that parameter)
print(model.kern.lengthscale)

'''
Input: Xp must be 2D array (rows = number of points to evaluate, cols =
parameters)
Returns: tuple, first element is the predictive mean, second element is
the predictive variance
'''

predict = lambda Xp: model.predict((Xp - X_m) / X_s)
Xp = np.loadtxt('150V_input.txt')
Mp, Vp = predict(Xp) # Mp is predictive mean, Vp is predictive variance
# Standard devivation
Sp = np.sqrt(Vp)
# Lower predictive bound
Lp = Mp - 1.96 * Sp
# Upper predictive bound
Up = Mp + 1.96 * Sp

np.savetxt('Mp_%sPC_150V.txt'%(i+1), Mp, fmt = '%.6f') # predictive mean  
np.savetxt('Sp_%sPC_150V.txt'%(i+1), Sp, fmt = '%.6f') # predictive standard variance

# First, I will fix the 1st and 2nd parameters,
# and I will plot the response as a function of the 3rd:
X3 = np.linspace(X[:, 2].min(), X[:, 2].max(), 100)[:, None]
# First 2 columns fixed to the mean, the third column X3:
Xp = np.hstack([np.matlib.repmat(X_m[0], X3.shape[0], 1), np.matlib.repmat(X_m[1], X3.shape[0], 1),X3])
Mp, Vp = predict(Xp) # Mp is predictive mean, Vp is predictive variance
# Standard devivation
Sp = np.sqrt(Vp)
# Lower predictive bound
Lp = Mp - 1.96 * Sp
# Upper predictive bound
Up = Mp + 1.96 * Sp

fig, ax = plt.subplots()
plt.plot(X3,Mp,color='red',label='Predictive mean')
ax.fill_between(X3.flatten(), Lp.flatten(), Up.flatten(), alpha=0.25, color='grey', label='95% predictive intervals')
ax.set_xlabel(r'$k_{2}$')
ax.set_ylabel('%s principal component score'%(i+1))
plt.title('%s principal component score'%(i+1))
plt.legend(loc='best')
plt.savefig('%sPC_prediction.png'%(i+1))    

# Now, let's draw the contours for fixed first parameter and varying
# second and third parameters
x2 = np.linspace(X[:, 0].min(), X[:, 0].max(), 32)
x3 = np.linspace(X[:, 1].min(), X[:, 1].max(), 32)
X2, X3 = np.meshgrid(x2, x3)
X23_all = np.hstack([X2.flatten()[:, None], X3.flatten()[:, None]])
Xp = np.hstack([X2.flatten()[:, None],X3.flatten()[:, None],np.matlib.repmat(X_m[2], X23_all.shape[0], 1)])
Mp, Vp = predict(Xp) # Mp is predictive mean, Vp is predictive variance
# Standard devivation
Sp = np.sqrt(Vp)
# Lower predictive bound
Lp = Mp - 1.96 * Sp
# Upper predictive bound
Up = Mp + 1.96 * Sp
fig, ax = plt.subplots()
c = ax.contourf(X2, X3, Mp[:,0].reshape(X2.shape))
ax.set_xlabel(r'$k_{1}$')
ax.set_ylabel(r'$k_{2}$')
cbar = plt.colorbar(c)
plt.title('%s principal component score'%(i+1))
plt.savefig('%sPC_2d_contour.png'%(i+1))
