import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from matplotlib.pyplot import *
import GPy # pip install GPy
import numpy.matlib
from sklearn.decomposition import PCA

plt.style.use('classic')
rcParams['mathtext.fontset'] = 'custom'
rcParams['mathtext.it'] = u'Arial:italic'
rcParams['mathtext.rm'] = u'Arial'

def PMF(): # probability mass function
	mu = np.array([0.04498, 4.909200, 76.641340]) # reference mean of mu,k1,k2
	sigma = 0.2/1.96*mu # make std=0.1*mu to indicate 97.5% probability
	
	for i in range(3):
		x = mu[i] + sigma[i] * np.random.randn(10000)
		U = mu[i] + 1.96*sigma[i]
		L = mu[i] - 1.96*sigma[i]

		fig, ax = plt.subplots()
		ax.plot([mu[i],mu[i]],[0,1],lw=2)
		ax.scatter(mu[i],1,s=50)
		plt.yticks(fontsize=30,fontname = "Arial")
		plt.xticks(fontsize=30,fontname = "Arial")
		ax.tick_params(axis='x', pad=15)
		ax.tick_params(axis='y', pad=15)
		ax.set_ylabel('Probability',fontsize=30,fontname = "Arial")

		if i == 0:
			ax.set_xlabel(r'$\mathit{x_{1}}$',fontsize=30,fontname = "Arial")
			ax.set_ylabel('Probability',fontsize=30,fontname = "Arial")
			
			plt.xlim([0.02,0.07])
			plt.ylim([0,1.2])
			plt.xticks([0.025,0.045,0.065],fontsize=30,fontname = "Arial")
			ax.set_aspect(0.05/1.2)
			fig.tight_layout()
			plt.savefig('x1_PMF_20%.png')

		elif i == 1:
			ax.set_xlabel(r'$\mathit{x_{2}}$',fontsize=30,fontname = "Arial")
			ax.set_ylabel('Probability',fontsize=30,fontname = "Arial")
			plt.xlim([2.5,7.5])
			plt.ylim([0,1.2])
			ax.set_aspect(5./1.2)
			fig.tight_layout()
			plt.savefig('x2_PMF_20%.png')

		elif i == 2:
			ax.set_xlabel(r'$\mathit{x_{3}}$',fontsize=30,fontname = "Arial")
			ax.set_ylabel('Probability',fontsize=30,fontname = "Arial")
			
			plt.xlim([40,115])
			plt.ylim([0,1.2])
			plt.xticks([40,60,80,100],fontsize=30,fontname = "Arial")
			ax.set_aspect((75.)/(1.2))
			fig.tight_layout()
			plt.savefig('x3_PMF_20%.png')


def ND(): # normal distribution
	mu = np.array([0.04498, 4.909200, 76.641340]) # reference mean of mu,k1,k2
	sigma = 0.2/1.96*mu # make std=0.1*mu to indicate 97.5% probability
	
	for i in range(3):
		x = mu[i] + sigma[i] * np.random.randn(10000)
		U = mu[i] + 1.96*sigma[i]
		L = mu[i] - 1.96*sigma[i]
		num_bins = 1000
		fig, ax = plt.subplots()
		n, bins, patches = ax.hist(x, num_bins, alpha = 0.25, normed=1)
		fig, ax = plt.subplots()
		y = mlab.normpdf(bins, mu[i], sigma[i])
		ax.plot(bins, y,'-', color = 'blue', lw=2)
		plt.yticks(fontsize=30,fontname = "Arial")
		plt.xticks(fontsize=30,fontname = "Arial")
		ax.tick_params(axis='x', pad=15)
		ax.tick_params(axis='y', pad=15)
		ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")


		if i == 0:
			plt.xlim([min(bins)-max(bins)*0.05,max(bins)*1.05])
			plt.ylim([0,max(y)*1.05])
			plt.xlim([0.02,0.07])
			plt.ylim([0,100])
			plt.xticks([0.025,0.045,0.065],fontsize=30,fontname = "Arial")
			print('x1,x',min(bins)-max(bins)*0.05,max(bins)*1.05)
			print('x1,y',0,max(y)*1.05)
			ax.set_aspect((0.05)/(100.))
			ax.set_xlabel(r'$\mathit{x_{1}}$',fontsize=30,fontname = "Arial")
			fig.tight_layout()
			plt.savefig('x1_ND_20%.png')
	

		elif i == 1:
			plt.xlim([min(bins)-max(bins)*0.05,max(bins)*1.05])
			plt.ylim([0,max(y)*1.05])
			plt.xlim([2.5,7.5])
			plt.ylim([0,0.9])
			ax.set_aspect((5.)/(0.9))
			ax.set_xlabel(r'$\mathit{x_{2}}$',fontsize=30,fontname = "Arial")
			fig.tight_layout()
			plt.savefig('x2_ND_20%.png')

		elif i ==2:

			plt.xlim([min(bins)-max(bins)*0.05,max(bins)*1.05])
			plt.ylim([0,max(y)*1.05])
			plt.xlim([40,115])
			plt.ylim([0,0.06])
			plt.xticks([40,60,80,100],fontsize=30,fontname = "Arial")
			ax.set_aspect((75.)/(0.06))
			ax.set_xlabel(r'$\mathit{x_{3}}$',fontsize=30,fontname = "Arial")
			fig.tight_layout()
			plt.savefig('x3_ND_20%.png')

def UD():
	mu = np.array([0.04498, 4.909200, 76.641340]) # reference mean of mu,k1,k2
	sigma = 0.2/1.96*mu # make std=0.1*mu to indicate 97.5% probability
	
	for i in range(3):
		x = mu[i] + sigma[i] * np.random.randn(10000)
		U = mu[i] + 1.96*sigma[i]
		L = mu[i] - 1.96*sigma[i]
		num_bins = 1000
		fig, ax = plt.subplots()
		plt.yticks(fontsize=30,fontname = "Arial")
		plt.xticks(fontsize=30,fontname = "Arial")
		ax.tick_params(axis='x', pad=15)
		ax.tick_params(axis='y', pad=15)
		ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")
		p = np.random.uniform(mu[i]-3.*sigma[i],mu[i]+3.*sigma[i],1000)

		if i == 0:
			ax.plot([max(p),min(p)],[1./(max(p)-min(p)),1./(max(p)-min(p))],lw=2)	
			ax.set_xlabel(r'$\mathit{x_{1}}$',fontsize=30,fontname = "Arial")

			### 20% of mean covers 99.7%
			plt.xlim([0.02,0.07])
			plt.ylim([0,100])
			plt.xticks([0.025,0.045,0.065],fontsize=30,fontname = "Arial")
			ax.set_aspect((0.05)/(100.))
			fig.tight_layout()
			plt.savefig('x1_UD_20%.png')


		if i == 1:
			ax.plot([max(p),min(p)],[1./(max(p)-min(p)),1./(max(p)-min(p))],lw=2)
			ax.set_xlabel(r'$\mathit{x_{2}}$',fontsize=30,fontname = "Arial")
			plt.xlim([2.5,7.5])
			plt.ylim([0,0.9])
			ax.set_aspect((5.)/(0.9))
			fig.tight_layout()
			plt.savefig('x2_UD_20%.png')

		if i == 2:
			ax.plot([max(p),min(p)],[1./(max(p)-min(p)),1./(max(p)-min(p))],lw=2)
			ax.set_xlabel(r'$\mathit{x_{3}}$',fontsize=30,fontname = "Arial")
			plt.xlim([40,115])
			plt.ylim([0,0.06])
			plt.xticks([40,60,80,100],fontsize=30,fontname = "Arial")
			ax.set_aspect((75.)/(0.06))
			fig.tight_layout()
			plt.savefig('x3_UD_20%.png')


def GPR_PMD():
	mean = np.array([0.04498, 4.9092, 76.64134]) # mean of mu, k1, k2 in HOG model
	std = mean*0.2/1.96 # 1.2 of mean becomes 95% confidence probability
	for i in range(3):
		Z = np.loadtxt('650T_score.txt')
		Z = Z[:,i]
		X = np.loadtxt('650T_input.txt')
		X[:,0] *=2 # change from C10 to mu	

		if Z.ndim == 1:
			Z = Z[:, None]
		X_m = np.mean(X, axis=0)
		X_s = np.std(X, axis=0)
		X_scaled = (X - X_m) / X_s
		k = GPy.kern.RBF(X_scaled.shape[1], ARD=True)
		model = GPy.models.GPRegression(X_scaled, Z, k)
		model.optimize()
		print('*' * 80)
		print('Optimized model:')
		print(model)
		print('Lengthscales:') # Correspond to the sensitivity of the output
		                      # to each input (the smaller the lengthscale
		                      # of a parameter the more sensitive the response
		                      # to that parameter)
		print(model.kern.lengthscale)

		predict = lambda Xp: model.predict((Xp - X_m) / X_s)
		

		if i == 0:
			num_bins = np.linspace(-16,-4,1001) # z1
		elif i == 1:
			num_bins = np.linspace(-3.5,-1.0,1001) # z2
		elif i == 2:
			num_bins = np.linspace(-1.2,0.8,1001) # z3
		
		### no uncertainty
		Xp = mean[None,:]
		Mp, Vp = predict(Xp)
		Sp = np.sqrt(Vp)

		fig, ax = plt.subplots()
		y = mlab.normpdf(num_bins, Mp, Sp)
		ax.plot(np.reshape(num_bins,1001), np.reshape(y,1001),'-', color = 'blue', lw=2)

		np.savetxt('mean_of_z'+str(i+1)+'_PMD.txt',y,fmt='%.6f')
		if i == 0:
		### z1
			ax.set_xlabel(r'$z_{1}$',fontsize=30,fontname = "Arial")
			ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")
			plt.xlim([-16,-4])
			plt.ylim([0,0.8])
			plt.xticks([-16,-13,-10,-7,-4],fontsize=30,fontname = "Arial")
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			ax.set_aspect((12.)/(0.8))
			plt.tight_layout()
			plt.savefig('z1_PMD_with_error.png')

		elif i == 1:
		### z2
			ax.set_xlabel(r'$z_{2}$',fontsize=30,fontname = "Arial")
			ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")
			plt.xlim([-3.5,-1.0])
			plt.ylim([0,4.5])
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			ax.set_aspect((2.5)/(4.5))
			plt.tight_layout()
			plt.savefig('z2_PMD_with_error.png')
		
		elif i == 2:
		### z3
			ax.set_xlabel(r'$z_{3}$',fontsize=30,fontname = "Arial")
			ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")
			plt.xlim([-1.2,0.8])
			plt.ylim([0,3.2])
			plt.xticks([-1.2,-0.7,-0.2,0.3,0.8],fontsize=30,fontname = "Arial")
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			ax.set_aspect((2.0)/(3.2))
			plt.tight_layout()
			plt.savefig('z3_PMD_with_error.png')
		

def GPR_ND():
	mean = np.array([0.04498, 4.9092, 76.64134]) # mean of mu, k1, k2 in HOG model
	std = mean*0.2/1.96 # 1.2 of mean becomes 95% confidence probability
	S = [0,0,0]
	
	p1 = mean[0] + std[0] * np.random.randn(1000)
	p2 = mean[1] + std[1] * np.random.randn(1000)
	p3 = mean[2] + std[2] * np.random.randn(1000)

	for i in range(3): # the number o principal component scores
		Z = np.loadtxt('650T_score.txt')
		Z = Z[:,i]
		X = np.loadtxt('650T_input.txt')
		X[:,0] *=2 # change from C10 to mu		

		if Z.ndim == 1:
			Z = Z[:, None]

		X_m = np.mean(X, axis=0)
		X_s = np.std(X, axis=0)
		X_scaled = (X - X_m) / X_s
		k = GPy.kern.RBF(X_scaled.shape[1], ARD=True)
		model = GPy.models.GPRegression(X_scaled, Z, k)
		model.optimize()
		print('*' * 80)
		print('%sth principal component score' %(i+1))
		print('Optimized model:')
		print(model)
		print('Lengthscales:') # Correspond to the sensitivity of the output
	                      # to each input (the smaller the lengthscale
	                      # of a parameter the more sensitive the response
	                      # to that parameter)
		print(model.kern.lengthscale)

		predict = lambda Xp: model.predict((Xp - X_m) / X_s)

		tmp = np.zeros((1000,1000)) # temporary storage for each PC score

		for j in range(1000):
			Xp = np.array([p1[j],p2[j],p3[j]])[None,:]   
			Mp, Vp = predict(Xp) # Mp is predictive mean, Vp is predictive variance
			Sp = np.sqrt(Vp)
			tmp[:,j]= Mp + Sp * np.random.randn(1000)
		S[i] = tmp
	S1 = S[0].flatten() # 1st PC score
	S2 = S[1].flatten() # 2nd PC score
	S3 = S[2].flatten() # 3rd PC score

	L = np.array([np.percentile(S1,2.5), np.percentile(S2,2.5), np.percentile(S3,2.5)]) # Lower predictive bound
	U = np.array([np.percentile(S1,97.5), np.percentile(S2,97.5), np.percentile(S3,97.5)]) # Upper predictive bound
	M = np.array([np.percentile(S1,50), np.percentile(S2,50), np.percentile(S3,50)]) # 50 percentile
	result = np.array([L,M,U])

	for k in range(3):
		num_bins = 30
		if k == 0:
			fig, ax = plt.subplots()
			n, bins, patches = ax.hist(S1, num_bins, alpha = 0.25, normed=1)
			ax.set_xlabel(r'$z_{1}$',fontsize=30,fontname = "Arial")
			ax.set_aspect((8)/(0.8))
			plt.xlim([-14,-6])
			plt.ylim([0,0.8])
			plt.xticks([-14,-12,-10,-8,-6],fontsize=30,fontname = "Arial")
			plt.yticks([0,0.2,0.4,0.6,0.8],fontsize=30,fontname = "Arial")
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			plt.tight_layout()
			plt.show()
		if k == 1:
			fig, ax = plt.subplots()
			n, bins, patches = ax.hist(S2, num_bins, alpha = 0.25, normed=1)
			ax.set_xlabel(r'$z_{2}$',fontsize=30,fontname = "Arial")
			ax.set_aspect((1.4)/(4.5))
			plt.xlim([-3,-1.6])
			plt.ylim([0,4.5])
			plt.xticks([-3,-2.6,-2.2,-1.8],fontsize=30,fontname = "Arial")
			plt.yticks([0,1,2,3,4],fontsize=30,fontname = "Arial")
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			plt.tight_layout()
			plt.show()
		if k == 2:
			fig, ax = plt.subplots()
			n, bins, patches = ax.hist(S3, num_bins, alpha = 0.25, normed=1)
			ax.set_xlabel(r'$z_{3}$',fontsize=30,fontname = "Arial")
			ax.set_aspect((1.6)/(3.2))
			plt.xlim([-1,0.6])
			plt.ylim([0,3.2])
			plt.xticks([-1,-0.6,-0.2,0.2,0.6],fontsize=30,fontname = "Arial")
			plt.yticks([0,0.8,1.6,2.4,3.2],fontsize=30,fontname = "Arial")
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			plt.tight_layout()
			plt.show()


def GPR_ND_w_error():
	mean = np.array([0.04498, 4.9092, 76.64134]) # mean of mu, k1, k2 in HOG model
	std = mean*0.2/1.96 # 1.2 of mean becomes 95% confidence probability
	S = [0,0,0]
	
	p1 = mean[0] + std[0] * np.random.randn(1000)
	p2 = mean[1] + std[1] * np.random.randn(1000)
	p3 = mean[2] + std[2] * np.random.randn(1000)

	for i in range(3): # the number of principal component scores
		Z = np.loadtxt('650T_score.txt')
		Z = Z[:,i]
		X = np.loadtxt('650T_input.txt')
		X[:,0] *=2 # change from C10 to mu	

		if Z.ndim == 1:
			Z = Z[:, None]

		X_m = np.mean(X, axis=0)
		X_s = np.std(X, axis=0)
		X_scaled = (X - X_m) / X_s
		k = GPy.kern.RBF(X_scaled.shape[1], ARD=True)
		model = GPy.models.GPRegression(X_scaled, Z, k)
		model.optimize()
		print('*' * 80)
		print('%sth principal component score' %(i+1))
		print('Optimized model:')
		print(model)
		print('Lengthscales:') # Correspond to the sensitivity of the output
	                      # to each input (the smaller the lengthscale
	                      # of a parameter the more sensitive the response
	                      # to that parameter)
		print(model.kern.lengthscale)

		predict = lambda Xp: model.predict((Xp - X_m) / X_s)

		tmp = np.zeros((1000,1000)) # temporary storage for each PC score
		
		if i == 0:
			num_bins = np.linspace(-16,-4,31) # z1
		elif i == 1:
			num_bins = np.linspace(-3.5,-1.0,31) # z2
		elif i == 2:
			num_bins = np.linspace(-1.2,0.8,31) # z3

		for ii in range(1000):
			for j in range(1000):
				Xp = np.array([p1[j],p2[j],p3[j]])[None,:]   
				Mp, Vp = predict(Xp) # Mp is predictive mean, Vp is predictive variance
				Sp = np.sqrt(Vp)
				tmp[j,ii] = Mp + Sp * np.random.randn()
		
		fig, ax = plt.subplots()
		binss = np.zeros((31,1000))
		nn = np.zeros((30,1000))
		for k in range(1000):
			n1, bins1, patches1 = ax.hist(tmp[:,k], num_bins, alpha = 0.25, normed=1)
			binss[:,k] = bins1
			nn[:,k] = n1
		
		fig, ax = plt.subplots()
		pd1 = np.percentile(nn,2.5,axis=1)
		pd1 = np.reshape(pd1,30)
		pd2 = np.percentile(nn,50,axis=1)
		pd2 = np.reshape(pd2,30)	
		pd3 = np.percentile(nn,97.5,axis=1)
		pd3 = np.reshape(pd3,30)
		ax.plot(0.5*(num_bins[1:]+num_bins[:-1]), pd2, lw=2,label='Mean of PDF')
		ax.fill_between(0.5*(num_bins[1:]+num_bins[:-1]).flatten(), pd1.flatten(), pd3.flatten(), alpha=0.25, color='blue', label="95% error bars")
		plt.legend(loc='upper left')
		np.savetxt('mean_of_PDF_z'+str(i+1)+'_ND.txt',pd2,fmt='%.6f')
		if i == 0:
		### z1
			ax.set_xlabel(r'$z_{1}$',fontsize=30,fontname = "Arial")
			ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")
			plt.xlim([-16,-4])
			plt.ylim([0,0.8])
			plt.xticks([-16,-13,-10,-7,-4],fontsize=30,fontname = "Arial")
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			ax.set_aspect((12.)/(0.8))
			plt.tight_layout()
			plt.savefig('z1_ND_with_error.png')

		elif i == 1:
		### z2
			ax.set_xlabel(r'$z_{2}$',fontsize=30,fontname = "Arial")
			ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")
			plt.xlim([-3.5,-1.0])
			plt.ylim([0,4.5])
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			ax.set_aspect((2.5)/(4.5))
			plt.tight_layout()
			plt.savefig('z2_ND_with_error.png')
		
		elif i == 2:
		### z3
			ax.set_xlabel(r'$z_{3}$',fontsize=30,fontname = "Arial")
			ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")
			plt.xlim([-1.2,0.8])
			plt.ylim([0,3.2])
			plt.xticks([-1.2,-0.7,-0.2,0.3,0.8],fontsize=30,fontname = "Arial")
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			ax.set_aspect((2.0)/(3.2))
			plt.tight_layout()
			plt.savefig('z3_ND_with_error.png')

def GPR_UD():
	mean = np.array([0.04498, 4.9092, 76.64134]) # mean of mu, k1, k2 in HOG model
	std = mean*0.2/1.96 # 1.2 of mean becomes 95% confidence probability
	S = [0,0,0]
	
	p1 = np.random.uniform(mean[0]-3.*std[0],mean[0]+3.*std[0],1000) # It covers 99% confidential range
	p2 = np.random.uniform(mean[1]-3.*std[1],mean[1]+3.*std[1],1000)
	p3 = np.random.uniform(mean[2]-3.*std[2],mean[2]+3.*std[2],1000)

	for i in range(3): # the number o principal component scores
		Z = np.loadtxt('650T_score.txt')
		Z = Z[:,i]
		X = np.loadtxt('650T_input.txt')
		X[:,0] *=2 # change from C10 to mu	

		if Z.ndim == 1:
			Z = Z[:, None]

		X_m = np.mean(X, axis=0)
		X_s = np.std(X, axis=0)
		X_scaled = (X - X_m) / X_s
		k = GPy.kern.RBF(X_scaled.shape[1], ARD=True)
		model = GPy.models.GPRegression(X_scaled, Z, k)
		model.optimize()
		print('*' * 80)
		print('%sth principal component score' %(i+1))
		print('Optimized model:')
		print(model)
		print('Lengthscales:') # Correspond to the sensitivity of the output
	                      # to each input (the smaller the lengthscale
	                      # of a parameter the more sensitive the response
	                      # to that parameter)
		print(model.kern.lengthscale)

		predict = lambda Xp: model.predict((Xp - X_m) / X_s)

		tmp = np.zeros((1000,1000)) # temporary storage for each PC score

		for j in range(1000):
			Xp = np.array([p1[j],p2[j],p3[j]])[None,:]   
			Mp, Vp = predict(Xp) # Mp is predictive mean, Vp is predictive variance
			Sp = np.sqrt(Vp)
			tmp[:,j]= Mp + Sp * np.random.randn(1000)
		S[i] = tmp
	S1 = S[0].flatten() # 1st PC score
	S2 = S[1].flatten() # 2nd PC score
	S3 = S[2].flatten() # 3rd PC score

	L = np.array([np.percentile(S1,2.5), np.percentile(S2,2.5), np.percentile(S3,2.5)]) # Lower predictive bound
	U = np.array([np.percentile(S1,97.5), np.percentile(S2,97.5), np.percentile(S3,97.5)]) # Upper predictive bound
	M = np.array([np.percentile(S1,50), np.percentile(S2,50), np.percentile(S3,50)]) # 50 percentile
	result = np.array([L,M,U])

	for k in range(3):
		num_bins = 30
		if k == 0:
			fig, ax = plt.subplots()
			n, bins, patches = ax.hist(S1, num_bins, alpha = 0.25, normed=1)
			ax.set_xlabel(r'$z_{1}$',fontsize=30,fontname = "Arial")
			ax.set_aspect((8)/(0.8))
			plt.xlim([-14,-6])
			plt.ylim([0,0.8])
			plt.xticks([-14,-12,-10,-8,-6],fontsize=30,fontname = "Arial")
			plt.yticks([0,0.2,0.4,0.6,0.8],fontsize=30,fontname = "Arial")
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			plt.tight_layout()
			plt.show()
		if k == 1:
			fig, ax = plt.subplots()
			n, bins, patches = ax.hist(S2, num_bins, alpha = 0.25, normed=1)
			ax.set_xlabel(r'$z_{2}$',fontsize=30,fontname = "Arial")
			ax.set_aspect((1.4)/(4.5))
			plt.xlim([-3,-1.6])
			plt.ylim([0,4.5])
			plt.xticks([-3,-2.6,-2.2,-1.8],fontsize=30,fontname = "Arial")
			plt.yticks([0,1,2,3,4],fontsize=30,fontname = "Arial")
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			plt.tight_layout()
			plt.show()
		if k == 2:
			fig, ax = plt.subplots()
			n, bins, patches = ax.hist(S3, num_bins, alpha = 0.25, normed=1)
			ax.set_xlabel(r'$z_{3}$',fontsize=30,fontname = "Arial")
			ax.set_aspect((1.6)/(3.2))
			plt.xlim([-1,0.6])
			plt.ylim([0,3.2])
			plt.xticks([-1,-0.6,-0.2,0.2,0.6],fontsize=30,fontname = "Arial")
			plt.yticks([0,0.8,1.6,2.4,3.2],fontsize=30,fontname = "Arial")
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			plt.tight_layout()
			plt.show()

def GPR_UD_w_error():
	mean = np.array([0.04498, 4.9092, 76.64134]) # mean of mu, k1, k2 in HOG model
	std = mean*0.2/1.96 # 1.2 of mean becomes 95% confidence probability

	p1 = np.random.uniform(mean[0]-3.*std[0],mean[0]+3.*std[0],1000)
	p2 = np.random.uniform(mean[1]-3.*std[1],mean[1]+3.*std[1],1000)
	p3 = np.random.uniform(mean[2]-3.*std[2],mean[2]+3.*std[2],1000)

	for i in range(3): # the number of principal component scores

		Z = np.loadtxt('650T_score.txt')
		Z = Z[:,i]
		X = np.loadtxt('650T_input.txt')
		X[:,0] *=2 # change from C10 to mu	

		if Z.ndim == 1:
			Z = Z[:, None]

		X_m = np.mean(X, axis=0)
		X_s = np.std(X, axis=0)
		X_scaled = (X - X_m) / X_s
		k = GPy.kern.RBF(X_scaled.shape[1], ARD=True)
		model = GPy.models.GPRegression(X_scaled, Z, k)
		model.optimize()
		print('*' * 80)
		print('%sth principal component score' %(i+1))
		print('Optimized model:')
		print(model)
		print('Lengthscales:') # Correspond to the sensitivity of the output
	                      # to each input (the smaller the lengthscale
	                      # of a parameter the more sensitive the response
	                      # to that parameter)
		print(model.kern.lengthscale)

		predict = lambda Xp: model.predict((Xp - X_m) / X_s)

		tmp = np.zeros((1000,1000)) # temporary storage for each PC score
		
		if i == 0:
			num_bins = np.linspace(-16,-4,31) # z1
		elif i == 1:
			num_bins = np.linspace(-3.5,-1.0,31) # z2
		elif i == 2:
			num_bins = np.linspace(-1.2,0.8,31) # z3

		for ii in range(1000):
			for j in range(1000):
				Xp = np.array([p1[j],p2[j],p3[j]])[None,:]   
				Mp, Vp = predict(Xp) # Mp is predictive mean, Vp is predictive variance
				Sp = np.sqrt(Vp)
				tmp[j,ii] = Mp + Sp * np.random.randn()
		
		fig, ax = plt.subplots()
		binss = np.zeros((31,1000))
		nn = np.zeros((30,1000))
		for k in range(1000):
			n1, bins1, patches1 = ax.hist(tmp[:,k], num_bins, alpha = 0.25, normed=1)
			binss[:,k] = bins1
			nn[:,k] = n1
		
		fig, ax = plt.subplots()
		pd1 = np.percentile(nn,2.5,axis=1)
		pd1 = np.reshape(pd1,30)
		pd2 = np.percentile(nn,50,axis=1)
		pd2 = np.reshape(pd2,30)
		pd3 = np.percentile(nn,97.5,axis=1)
		pd3 = np.reshape(pd3,30)
		ax.plot(0.5*(num_bins[1:]+num_bins[:-1]), pd2, lw=2,label='Mean of PDF')
		ax.fill_between(0.5*(num_bins[1:]+num_bins[:-1]).flatten(), pd1.flatten(), pd3.flatten(), alpha=0.25, color='blue', label="95% error bars")
		plt.legend(loc='upper left')
		np.savetxt('mean_of_PDF_z'+str(i+1)+'_UD.txt',pd2,fmt='%.6f')
		if i == 0:
		### z1
			ax.set_xlabel(r'$z_{1}$',fontsize=30,fontname = "Arial")
			ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")
			plt.xlim([-16,-4])
			plt.ylim([0,0.8])
			plt.xticks([-16,-13,-10,-7,-4],fontsize=30,fontname = "Arial")
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			ax.set_aspect((12.)/(0.8))
			plt.tight_layout()
			plt.savefig('z1_UD_with_error.png')

		elif i == 1:
		### z2
			ax.set_xlabel(r'$z_{2}$',fontsize=30,fontname = "Arial")
			ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")
			plt.xlim([-3.5,-1.0])
			plt.ylim([0,4.5])
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			ax.set_aspect((2.5)/(4.5))
			plt.tight_layout()
			plt.savefig('z2_UD_with_error.png')
		
		elif i == 2:
		### z3
			ax.set_xlabel(r'$z_{3}$',fontsize=30,fontname = "Arial")
			ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")
			plt.xlim([-1.2,0.8])
			plt.ylim([0,3.2])
			plt.xticks([-1.2,-0.7,-0.2,0.3,0.8],fontsize=30,fontname = "Arial")
			plt.yticks(fontsize=30,fontname = "Arial")
			plt.xticks(fontsize=30,fontname = "Arial")
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			ax.set_aspect((2.0)/(3.2))
			plt.tight_layout()
			plt.savefig('z3_UD_with_error.png')


def GPR_temporal_tjunction():
	mean = np.array([0.04498, 4.9092, 76.64134]) # mean of mu, k1, k2 in HOG model
	mean[0] = 0.022491 # convert mu to C10
	std = mean*0.2/1.96 # 1.2 of mean becomes 95% confidence probability
	Z = np.loadtxt('tjunction_nodal_stress_avg.txt')
	X = np.loadtxt('650T_input.txt')
	if Z.ndim == 1:
		Z = Z[:, None]
	X_m = np.mean(X, axis=0)
	X_s = np.std(X, axis=0)
	X_scaled = (X - X_m) / X_s
	k = GPy.kern.RBF(X_scaled.shape[1], ARD=True)
	model = GPy.models.GPRegression(X_scaled, Z, k)
	model.optimize()
	print('*' * 80)
	print('Optimized model:')
	print(model)
	print('Lengthscales:') # Correspond to the sensitivity of the output
	                      # to each input (the smaller the lengthscale
	                      # of a parameter the more sensitive the response
	                      # to that parameter)
	print(model.kern.lengthscale)

	predict = lambda Xp: model.predict((Xp - X_m) / X_s)
	
	'''
	### no uncertainty
	Xp = mean[None,:]
	Mp, Vp = predict(Xp)
	Sp = np.sqrt(Vp)
	Mp = Mp[0,0]
	Sp = Sp[0,0]

	x = Mp + Sp * np.random.randn(10000)
	num_bins = 100
	fig, ax = plt.subplots()
	n, bins, patches = ax.hist(x, num_bins, alpha = 0.25, normed=1)
	fig, ax = plt.subplots()

	y = mlab.normpdf(bins, Mp, Sp)
	ax.plot(bins, y,'-', color = 'blue', lw=2)
	ax.set_aspect((0.25)/(45.))
	plt.xlim([0.1,0.35])
	plt.ylim([0,45])
	#ax.set_aspect((0.09)/(150.))
	#plt.xlim([0.06,0.15])
	#plt.ylim([0,150])
	ax.set_xlabel('Average nodal stress (MPa)',fontsize=30,fontname = "Arial")
	ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")
	plt.yticks(fontsize=30,fontname = "Arial")
	plt.xticks(fontsize=30,fontname = "Arial")
	ax.tick_params(axis='x', pad=15)
	ax.tick_params(axis='y', pad=15)
	plt.tight_layout()
	plt.savefig('temporal_avg_NS_material_no_uncertainty.png')
	#plt.show()
	'''

	### uniform distribution
	x1 = np.random.uniform(mean[0]-3.*std[0],mean[0]+3.*std[0],1000) # It covers 99% confidential range
	x2 = np.random.uniform(mean[1]-3.*std[1],mean[1]+3.*std[1],1000)
	x3 = np.random.uniform(mean[2]-3.*std[2],mean[2]+3.*std[2],1000)
	tmp = np.zeros((1000,1000)) # temporary storage for each PC score

	#num_bins = np.linspace(0.1,0.35,31) # temporal
	num_bins = np.linspace(0.06,0.15,31) # t-junction


	for ii in range(1000):
		for j in range(1000):
			Xp = np.array([x1[j],x2[j],x3[j]])[None,:]   
			Mp, Vp = predict(Xp) # Mp is predictive mean, Vp is predictive variance
			Sp = np.sqrt(Vp)
			tmp[j,ii] = Mp + Sp * np.random.randn()
	
	fig, ax = plt.subplots()
	binss = np.zeros((31,1000))
	nn = np.zeros((30,1000))

	for k in range(1000):
		n1, bins1, patches1 = ax.hist(tmp[:,k], num_bins, alpha = 0.25, normed=1)
		binss[:,k] = bins1
		nn[:,k] = n1

	fig, ax = plt.subplots()
	pd1 = np.percentile(nn,2.5,axis=1)
	pd1 = np.reshape(pd1,30)
	pd2 = np.percentile(nn,50,axis=1)
	pd2 = np.reshape(pd2,30)
	pd3 = np.percentile(nn,97.5,axis=1)
	pd3 = np.reshape(pd3,30)

	np.savetxt('mean_of_PDF_UD_tjunction.txt',pd2,fmt='%.6f')

	ax.plot(0.5*(num_bins[1:]+num_bins[:-1]), pd2, lw=2,label='Mean of PDF')
	ax.fill_between(0.5*(num_bins[1:]+num_bins[:-1]).flatten(), pd1.flatten(), pd3.flatten(), alpha=0.25, color='blue', label="95% error bars")
	
	plt.legend(loc='upper left')
	ax.set_xlabel('Average nodal stress (MPa)',fontsize=30,fontname = "Arial")
	ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")
	plt.yticks(fontsize=30,fontname = "Arial")
	plt.xticks(fontsize=30,fontname = "Arial")
	ax.tick_params(axis='x', pad=15)
	ax.tick_params(axis='y', pad=15)
	### temporal
	#ax.set_aspect((0.25)/(45.))
	#plt.xlim([0.1,0.35])
	#plt.ylim([0,45])
	###
	### t-junction
	ax.set_aspect((0.09)/(150.))
	plt.xlim([0.06,0.15])
	plt.ylim([0,150])
	###
	plt.tight_layout()
	plt.savefig('tjunction_avg_NS_material_uncertainty_uniform.png')
	
	### normal distribution
	x1 = mean[0] + std[0] * np.random.randn(1000)
	x2 = mean[1] + std[1] * np.random.randn(1000)
	x3 = mean[2] + std[2] * np.random.randn(1000)

	tmp = np.zeros((1000,1000)) # temporary storage for each PC score

	for ii in range(1000):
		for j in range(1000):
			Xp = np.array([x1[j],x2[j],x3[j]])[None,:]   
			Mp, Vp = predict(Xp) # Mp is predictive mean, Vp is predictive variance
			Sp = np.sqrt(Vp)
			tmp[j,ii] = Mp + Sp * np.random.randn()
	
	fig, ax = plt.subplots()
	binss = np.zeros((31,1000))
	nn = np.zeros((30,1000))

	for k in range(1000):
		n1, bins1, patches1 = ax.hist(tmp[:,k], num_bins, alpha = 0.25, normed=1)
		binss[:,k] = bins1
		nn[:,k] = n1

	fig, ax = plt.subplots()
	pd1 = np.percentile(nn,2.5,axis=1)
	pd1 = np.reshape(pd1,30)
	pd2 = np.percentile(nn,50,axis=1)
	pd2 = np.reshape(pd2,30)
	pd3 = np.percentile(nn,97.5,axis=1)
	pd3 = np.reshape(pd3,30)

	np.savetxt('mean_of_PDF_ND_tjunction.txt',pd2,fmt='%.6f')

	ax.plot(0.5*(num_bins[1:]+num_bins[:-1]), pd2, lw=2,label='Mean of PDF')
	ax.fill_between(0.5*(num_bins[1:]+num_bins[:-1]).flatten(), pd1.flatten(), pd3.flatten(), alpha=0.25, color='blue', label="95% error bars")
	
	plt.legend(loc='upper left')
	ax.set_xlabel('Average nodal stress (MPa)',fontsize=30,fontname = "Arial")
	ax.set_ylabel('Probability density',fontsize=30,fontname = "Arial")
	plt.yticks(fontsize=30,fontname = "Arial")
	plt.xticks(fontsize=30,fontname = "Arial")
	ax.tick_params(axis='x', pad=15)
	ax.tick_params(axis='y', pad=15)
	### temporal
	#ax.set_aspect((0.25)/(45.))
	#plt.xlim([0.1,0.35])
	#plt.ylim([0,45])
	###
	### t-junction
	ax.set_aspect((0.09)/(150.))
	plt.xlim([0.06,0.15])
	plt.ylim([0,150])
	###
	plt.tight_layout()
	plt.savefig('tjunctioin_avg_NS_material_uncertainty_normal.png')
	

def invPCA():
	data=np.loadtxt('650T_stress.txt')
	z_ND = np.loadtxt('material_ND_2p5_50_97p5_z_surrogate.txt')
	z_UD = np.loadtxt('material_UD_2p5_50_97p5_z_surrogate.txt')
	z_l = z_ND[0,:]
	z_m = z_ND[1,:]
	z_u = z_ND[2,:]
	
	pca = PCA(n_components=3)
	pca.fit(data)
	print(z_l,z_m,z_u)
	data_new = pca.inverse_transform(z_l)
	np.savetxt('mean_of_PDF_ND_surrogate_NS_2p5.txt',data_new,fmt='%.6f')
	data_new = pca.inverse_transform(z_m)
	np.savetxt('mean_of_PDF_ND_surrogate_NS_50.txt',data_new,fmt='%.6f')
	data_new = pca.inverse_transform(z_u)
	np.savetxt('mean_of_PDF_ND_surrogate_NS_97p5.txt',data_new,fmt='%.6f')


if __name__ == '__main__':
	option = sys.argv[1]
	if option == 'GPR_PMD':
		GPR_PMD()

	if option == 'GPR_ND_w_error':
		GPR_ND_w_error()

	if option == 'GPR_UD_w_error':
		GPR_UD_w_error()

	if option == 'GPR_temporal_tjunction':
		GPR_temporal_tjunction()
